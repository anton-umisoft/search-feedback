<?php
$dir = __DIR__ . "/../../data/host/$options[name]/root";
echo "Set current dir to $dir\n";
chdir($dir); // <---------------- MAGIC STARTS HERE 

$objects = new selector('objects');
$objects->limit(0, 1);
$objects->types('object-type')->guid('umiruguid-type-author-info');

if ($objects->length() > 0) {
 
	$obj = array_shift($objects->result());
	if ($obj instanceof umiObject) {
		$img = $obj->getValue('order_button_img');
		
		if ($img instanceof umiImageFile) {
			echo "Founded not original 'order_button_img': $img\n";
			
			file_put_contents(__DIR__ . '/founded.log', "$options[name],$img\n", FILE_APPEND);
		}
	}
	
} else {
	echo "umiruguid-type-author-info has not found.";
}


